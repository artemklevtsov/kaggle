#' @title Submit competition
#'
#' @param id Competition name.
#' @param file Competition submission file.
#' @param description Description of competition submission.
#'
#' @note
#' You will need to accept competition rules at <https://www.kaggle.com/c/<competition-name>/rules>.
#'
#' @family kaggle_competitions
#'
#' @examples
#' \dontrun{
#' competition_submit("titanic", "submit.csv", "My best model")
#' }
#'
#' #' @import checkmate
#' @export
#'
competition_submit <- function(id, file, description) {
  checkmate::assert_string(id)
  checkmate::assert_file_exists(file, access = "r")
  checkmate::assert_string(description)

  file_name <- basename(file)
  content_length <- file.size(file)
  last_modified <- last_modified_utc(file)

  # Get upload URL and token
  res_url <- submission_url_get(
    id = id,
    file_name = file_name,
    content_length = content_length,
    last_modified = last_modified
  )
  assert_upload(res_url)

  #   create_url <- strsplit(res_url$createUrl, split = "/", fixed = TRUE)[[1L]]
  #   guid <- create_url[length(create_url) - 2L]
  #   res_upload <- submission_upload(file = file, guid = guid, content_length = content_length, last_modified = last_modified)
  #   upload_token <- res_upload$token

  # Upload file
  is_uploaded <- upload_file(res_url[["createUrl"]], file)
  if (isFALSE(is_uploaded)) {
    msg <- sprintf("Could not submit '%s' file to the '%s' competition.", file, id)
    stop(msg, call. = FALSE)
  }

  # Submit uploading
  res_submit <- submission_submit(
    id = id,
    token = res_url[["token"]],
    description = description
  )

  message(res_submit$message)
  return(invisible(TRUE))
}


#' @title Generate competition submission URL
#'
#' @param id Competition name.
#' @param file_name Competition submission file name.
#' @param content_length Content length of file in bytes.
#' @param last_modified Last modified date of file in milliseconds since epoch in UTC.
#'
#' @noRd
#'
#' @import checkmate
#'
submission_url_get <- function(id, file_name, content_length, last_modified) {
  checkmate::assert_string(id)
  checkmate::assert_string(file_name)
  checkmate::assert_number(content_length, lower = 0)
  checkmate::assert_number(last_modified)

  path <- paste("competitions", id, "submissions/url", content_length, last_modified, sep = "/")
  body <- list(fileName = file_name)
  api_post_form(path = path, body = body)
}

#' @title Submit to competition
#'
#' @param id Competition name.
#' @param token Token identifying location of uploaded submission file.
#' @param description Description of competition submission.
#'
#' @noRd
#'
#' @import checkmate
#'
submission_submit <- function(id, token, description) {
  checkmate::assert_string(id)
  checkmate::assert_string(token)
  checkmate::assert_string(description)

  path <- paste("competitions/submissions/submit", id, sep = "/")
  body <- list(
    blobFileTokens = token,
    submissionDescription = description
  )

  api_post_form(path = path, body = body)
}

#' @title Upload competition submission file
#'
#' @param file Competition submission file.
#' @param guid Location where submission should be uploaded.
#' @param content_length Content length of file in bytes.
#' @param last_modified Last modified date of file in milliseconds since epoch in UTC.
#'
#' @noRd
#'
#' @import checkmate
#' @import httr
#'
submission_upload <- function(file, guid, content_length, last_modified) {
  checkmate::assert_file_exists(file, access = "r")
  checkmate::assert_string(guid)
  checkmate::assert_number(content_length, lower = 0)
  checkmate::assert_number(last_modified)

  path <- paste("competitions/submissions/upload", guid, content_length, last_modified, sep = "/")
  body <- list(file = httr::upload_file(file))
  api_post_form(path = path, body = body)
}
