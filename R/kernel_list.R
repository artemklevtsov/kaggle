#' @title List kernels
#'
#' @param group Display only your kernels.
#' @param user isplay kernels by a particular group.
#' @param language Display kernels in a specific language.
#' @param kernel_type Display kernels of a specific type.
#' @param output_type Display kernels with a specific output type.
#' @param dataset Display kernels using the specified dataset.
#' @param competition Display kernels using the specified competition.
#' @param parent_kernel Display kernels that have forked the specified kernel.
#' @param sort_by Sort the results. 'relevance' only works if there is a search query.
#' @param search Search terms.
#' @param page Page number.
#' @param page_size Page size.
#'
#' @family kaggle_kernels
#'
#' @export
#'
kernel_list <- function(group = NULL, user = NULL, language = NULL, kernel_type = NULL, output_type = NULL, dataset = NULL, competition = NULL, parent_kernel = NULL, sort_by = NULL, search = NULL, page = NULL, page_size = NULL) {
  checkmate::assert_string(group, null.ok = TRUE)
  checkmate::assert_string(user, null.ok = TRUE)
  checkmate::assert_string(language, null.ok = TRUE)
  checkmate::assert_string(kernel_type, null.ok = TRUE)
  checkmate::assert_string(output_type, null.ok = TRUE)
  checkmate::assert_string(dataset, null.ok = TRUE)
  checkmate::assert_string(competition, null.ok = TRUE)
  checkmate::assert_string(parent_kernel, null.ok = TRUE)
  checkmate::assert_string(sort_by, null.ok = TRUE)
  checkmate::assert_string(search, null.ok = TRUE)
  checkmate::assert_number(page, lower = 1L, null.ok = TRUE)
  checkmate::assert_number(page_size, lower = 1L, null.ok = TRUE)
  checkmate::assert_choice(group, c("everyone", "profile", "upvoted"), null.ok = TRUE)
  checkmate::assert_choice(language, c("all", "python", "r", "sqlite", "julia"), null.ok = TRUE)
  checkmate::assert_choice(kernel_type, c("all", "script", "notebook"), null.ok = TRUE)
  checkmate::assert_choice(output_type, c("all", "visualization", "data"), null.ok = TRUE)
  checkmate::assert_choice(sort_by, c("hotness", "commentCount", "dateCreated", "dateRun", "scoreAscending", "scoreDescending", "viewCount", "voteCount", "relevance"), null.ok = TRUE)

  path <- "kernels/list"
  query <- list(
    group = group,
    user = user,
    language = language,
    kernelType = kernel_type,
    outputType = output_type,
    dataset = dataset,
    competition = competition,
    parentKernel = parent_kernel,
    sortBy = sort_by,
    search = search,
    page = page,
    page_size = page_size
  )
  api_get_json(path = path, query = query)
}
