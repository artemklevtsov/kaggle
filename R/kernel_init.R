#' @title Initialize metadata file for a kernel
#'
#' @param path  Folder for upload, containing data files and a special kernel-metadata.json file.
#'
#' @family kaggle_kernels
#'
#' @references
#' <https://github.com/Kaggle/kaggle-api/wiki/Kernel-Metadata>
#'
#' @examples
#' \dontrun{
#' dir.create("code")
#' dataset_init("code")
#' }
#' 
#' @export
#'
kernel_init <- function(path) {
  checkmate::assert_directory_exists(path, access = "w")
  checkmate::assert_string(.pkgenv$user, .var.name = "Kaggle user")

  meta <- list(
    id = paste(.pkgenv$user, "INSERT_SLUG_HERE", sep = "/"),
    title = "INSERT_TITLE_HERE",
    code_file = "INSERT_CODE_FILE_PATH_HERE",
    language = "INSERT_LANGUAGE_HERE",
    kernel_type = "INSERT_KERNEL_TYPE_HERE",
    is_private = TRUE,
    enable_gpu = FALSE,
    enable_internet = FALSE,
    dataset_sources = list(),
    competition_sources = list(),
    kernel_sources = list()
  )
  jsonlite::write_json(
    x = meta,
    path = file.path(path, getOption("kaggle.kernel_metadata_file")),
    auto_unbox = TRUE,
    pretty = 2
  )
}
