#' @title Get dataset creation status
#'
#' @param owner Dataset owner.
#' @param dataset Dataset name.
#'
#' @family kaggle_datasets
#'
#' @examples
#' \dontrun{
#' dataset_status("mlg-ulb", "creditcardfraud")
#' dataset_status("uciml", "iris")
#' }
#' 
#' @import checkmate
#'
#' @export
#'
dataset_status <- function(owner, dataset) {
  checkmate::assert_string(owner)
  checkmate::assert_string(dataset)

  path <- paste("datasets/status", owner, dataset, sep = "/")
  api_get_json(path = path)
}
