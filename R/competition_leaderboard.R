#' @title Competition leaderboard
#'
#' @param id Competition name.
#' @param dest_file Path to destination file.
#' @param full Should be full leaderboard downloaded?
#'
#' @note
#' You will need to accept competition rules at <https://www.kaggle.com/c/<competition-name>/rules>.
#'
#' @family kaggle_competitions
#'
#' @examples
#' \dontrun{
#' # Get first 50 records
#' competition_leaderboard("titanic")
#' # Get full leaderboard
#' competition_leaderboard("titanic", full = TRUE)
#' }
#'
#' @import checkmate
#'
#' @export
#'
competition_leaderboard <- function(id, full = FALSE) {
  checkmate::assert_string(id)
  checkmate::assert_flag(full)

  if (isTRUE(full)) {
    tmp <- tempfile(fileext = ".zip")
    on.exit(unlink(tmp))
    leaderboard_download(id, tmp)
    res <- read.table(
      file = unz(tmp, paste0(id, "-publicleaderboard.csv")),
      sep = ",",
      header = TRUE,
      quote = "\"",
      comment.char = "",
      col.names = c("teamId", "teamName", "submissionDate", "score"),
      stringsAsFactors = FALSE
    )
  } else {
    res <- leaderboard_view(id)$submissions
  }

  # Post process
  if (length(res) > 0) {
    res[["score"]] <- as.numeric(res[["score"]])
    res[["submissionDate"]] <- parse_datetime(res[["submissionDate"]])
  }

  return(res)
}


#' @title Download competition leaderboard
#'
#' @param id Competition name.
#' @param dest_file Path to destination file.
#'
#' @noRd
#'
#' @import checkmate
#'
leaderboard_download <- function(id, dest_file) {
  checkmate::assert_string(id)
  if (missing(dest_file)) {
    dest_file <- file.path(getwd(), paste0(id, ".zip"))
  }
  checkmate::assert_path_for_output(dest_file, overwrite = TRUE)

  path <- paste("competitions", id, "leaderboard/download", sep = "/")
  api_get_file(path = path, dest_file = dest_file)
}

#' @title View competition leaderboard
#'
#' @param id Competition name.
#'
#' @noRd
#'
#' @import checkmate
#'
leaderboard_view <- function(id) {
  checkmate::assert_string(id)

  path <- paste("competitions", id, "leaderboard/view", sep = "/")
  api_get_json(path = path)
}
